#!/usr/bin/env bash

set -euo pipefail
SCRIPT_DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"
UNSYMLINKED_SCRIPT_DIR="$(readlink -f "${SCRIPT_DIR}" || readlink "${SCRIPT_DIR}" || echo "${SCRIPT_DIR}")"
# shellcheck disable=SC1091,SC1090
source "${UNSYMLINKED_SCRIPT_DIR}/../../../../workflow-script-commons.sh"

if [[ -z $POSTGRESQL_GCP_WAL_SECONDARY ]]; then
  echo "You must set POSTGRESQL_GCP_WAL_SECONDARY in source_vars"
fi
ssh_host "$POSTGRESQL_GCP_WAL_SECONDARY" "sudo gitlab-psql -d gitlab_repmgr -c 'select name,priority from repmgr_gitlab_cluster.repl_nodes'"
